const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const game = require("./oop.js");
const questionRouter = require("./routes/question.routes");

const app = express();

mongoose.connect("mongodb://localhost:27017/school_app");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static(__dirname + "/assets/"))
app.use("/question", questionRouter);

app.get("/", (req, res) => {
    res.send("Welcome to Batch B");
})

app.get("/registration-page", (req, res) => {
    res.sendFile(__dirname + "/html/index.html");
})

app.get("/game", (req, res) => {
    const newGame = new game(req.query.name);
    newGame.sayHello();
    newGame.start();
    res.send("Your game has began");
    console.log("User tried something")
})

app.get("/registration", (req, res) => {
    if(req.query.fname != "" && req.query.lname != "") {
        res.send("Registration Successful")
    } else {
        res.send("Registration Failed!")
    }
    
})

app.post("/registration", (req, res) => {
    if(req.body.fname != "" && req.body.lname != "") {
        res.send("Registration Successful")
    } else {
        res.send("Registration Failed!")
    }
})

app.listen(5000, console.log("Hurray!!! Server is running on port 5000"));

