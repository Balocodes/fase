// this is the model for questions

const mongoose = require("mongoose");

const schema = mongoose.Schema;

const questionSchema = new schema({
    question: { type: String },
    options: {
        option_a: { type: String },
        option_b: { type: String },
        option_c: { type: String }
    },
    correct_answer: { type: String }, // values can be a, b or c
    created_at: { type: Date, default: Date.now, once: true },
    updated_at: { type: Date, default: Date.now }
});

const questionModel = mongoose.model("Question", questionSchema);

module.exports = questionModel;